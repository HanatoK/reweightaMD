#ifndef REWEIGHTGRID_H
#define REWEIGHTGRID_H
#include "Axis.h"
#include <vector>
#include <string>
#include <cstddef>
#include <algorithm>
#include <numeric>
#include <cstdio>
#include <cstring>
#include <iostream>

using std::vector;
using std::string;
using std::begin;
using std::end;
using std::cout;

size_t factorial(size_t n) {
    if (n == 0) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}

// C(k, n)
size_t combination(size_t n, size_t k) {
    return factorial(n)/(factorial(k) * factorial(n-k));
}

class Value
{
public:
    Value();
    Value(size_t ndims);
    Value(size_t ndims, double init);
    bool empty() const;
    void addValue(const vector<double>& value, double weight = 1.0);
    const vector<double>& getData() const;
    vector<double> getData();
    void multiply(double constant);
    double& operator[](size_t i) { return mData[i]; }
private:
    vector<double>          mData;
};

Value::Value(): mData(0) {}
Value::Value(size_t ndims): mData(ndims, 0.0) {}
Value::Value(size_t ndims, double init): mData(ndims, init) {}

bool Value::empty() const {
    return mData.empty();
}

void Value::addValue(const vector<double>& value, double weight) {
    auto it_value = value.cbegin();
    auto it_mData = mData.begin();
    while (it_mData != mData.end()) {
        (*it_mData) += (*it_value) * weight;
        ++it_mData;
        ++it_value;
    }
}

const vector<double>& Value::getData() const {
    return mData;
}

vector<double> Value::getData() {
    return mData;
}

void Value::multiply(double constant) {
    for (auto &i : mData) i *= constant;
}

class AmdGrid
{
public:
    AmdGrid();
    AmdGrid(const vector<Axis>& ax);
    bool isInGrid(const vector<double>& pos) const;
    bool index(const vector<double>& pos, vector<size_t>& idx) const;
    bool address(const vector<double>& pos, size_t& addr) const;
    bool store(const vector<double>& pos, const vector<double>& grad, double weight);
    bool getValue(const vector<double>& pos, Value& res) const;
    void writeGrad(FILE* fout) const;
    void writeCount(FILE* fout) const;
private:
    size_t                  mNDim;
    vector<Axis>            mAxes;
    vector<size_t>          mAccu;
    size_t                  mGridSize;
    vector<double>          mWeightSum;
    vector<size_t>          mCount;
    vector<Value>           mGradient;
    vector<vector<double>>  mPointTable;

    void fillTable();
};

AmdGrid::AmdGrid() {}

AmdGrid::AmdGrid(const vector<Axis>& ax):
mNDim(ax.size()), mAxes(ax), mAccu(mNDim) {
    mAccu[0] = 1;
    mGridSize = 1;
    for (size_t i = 0; i < mNDim; ++i) {
        mAccu[i] = (i == 0) ? 1 : (mAccu[i - 1] * mAxes[i - 1].bin());
        mGridSize *= mAxes[i].bin();
    }
    mWeightSum.assign(mGridSize, 0.0);
    Value tmp(mNDim);
    mGradient.assign(mGridSize, tmp);
    mCount.assign(mGridSize, 0);
    fillTable();
}

bool AmdGrid::isInGrid(const vector<double>& pos) const {
    auto it_val = pos.cbegin();
    auto it_ax = mAxes.cbegin();
    while (it_ax != mAxes.cend()) {
        if (!(it_ax->isInBoundary((*it_val)))) return false;
    }
    return true;
}

bool AmdGrid::index(const vector<double>& pos, vector<size_t>& idx) const {
    idx.assign(mNDim, 0);
    auto it_val = pos.cbegin();
    auto it_ax = mAxes.cbegin();
    auto it_idx = idx.begin();
    while (it_idx != idx.end()) {
        if (!(it_ax->index((*it_val), (*it_idx)))) return false;
    }
    return true;
}

bool AmdGrid::address(const vector<double>& pos, size_t& addr) const {
    addr = 0;
    for (size_t i = 0; i < mNDim; ++i) {
        size_t idx_i = 0;
        if (!(mAxes[i].index(pos[i], idx_i))) return false;
        addr += idx_i * mAccu[i];
    }
    return true;
}

bool AmdGrid::store(const vector<double>& pos, const vector<double>& grad, double weight) {
    size_t addr = 0;
    bool inGrid = address(pos, addr);
    if (inGrid) {
        mGradient[addr].addValue(grad, weight);
        mWeightSum[addr] += weight;
        ++(mCount[addr]);
    }
    return inGrid;
}

bool AmdGrid::getValue(const vector<double>& pos, Value& res) const {
    size_t addr = 0;
    bool inGrid = address(pos, addr);
    if (inGrid) res = mGradient[addr];
    return inGrid;
}

void AmdGrid::writeGrad(FILE* fout) const {
    fprintf(fout, "# %ld\n", mNDim);
    for (const auto &ax : mAxes) {
        fprintf(fout, "%s\n", ax.infoHeader(1).c_str());
    }
    vector<double> pos(mNDim, 0.0);
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
            fprintf(fout, " %.7f", pos[j]);
        }
        size_t addr = 0;
        address(pos, addr);
        Value tmp_val = mGradient[addr];
        const double& tmp_wsum = mWeightSum[addr];
        if (tmp_wsum != 0) {
            tmp_val.multiply(-1.0 / tmp_wsum);
        }
        for (size_t j = 0; j < mNDim; ++j) {
            fprintf(fout, " %.9f", tmp_val[j]);
        }
        fprintf(fout, "\n");
    }
}

void AmdGrid::writeCount(FILE* fout) const {
    fprintf(fout, "# %ld\n", mNDim);
    for (const auto &ax : mAxes) {
        fprintf(fout, "%s\n", ax.infoHeader(1).c_str());
    }
    vector<double> pos(mNDim, 0.0);
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
            fprintf(fout, " %.7f", pos[j]);
        }
        size_t addr = 0;
        address(pos, addr);
        const size_t& count = mCount[addr];
        fprintf(fout, " %lu\n", count);
    }
}

void AmdGrid::fillTable() {
    vector<vector<double>> middlePoint(mNDim);
    for (size_t i = 0; i < mNDim; ++i) {
        middlePoint[i] = mAxes[i].middlePoint();
    }
    mPointTable.assign(mNDim, vector<double>(mGridSize, 0.0));
    for (size_t i = 0; i < mNDim; ++i) {
        size_t repeatAll = 1, repeatOne = 1;
        for (size_t j = i + 1; j < mNDim; ++j) {
            repeatOne *= middlePoint[j].size();
        }
        for (size_t j = 0; j < i; ++j) {
            repeatAll *= middlePoint[j].size();
        }
        const size_t in_i_sz = middlePoint[i].size();
        for (size_t l = 0; l < in_i_sz; ++l) {
            std::fill_n(begin(mPointTable[i]) + l * repeatOne, repeatOne, middlePoint[i][l]);
        }
        for (size_t k = 0; k < repeatAll - 1; ++k) {
        std::copy_n(begin(mPointTable[i]), repeatOne * in_i_sz,
                    begin(mPointTable[i]) + repeatOne * in_i_sz * (k + 1));
        }
    }
}

class Histogram
{
public:
    Histogram();
    Histogram(const vector<Axis>& ax);
    bool isInGrid(const vector<double>& pos) const;
    bool index(const vector<double>& pos, vector<size_t>& idx) const;
    bool address(const vector<double>& pos, size_t& addr) const;
    virtual bool store(const vector<double>& pos, double value = 1.0, double weight = 1.0);
    virtual bool getValue(const vector<double>& pos, double& res) const;
    virtual void writePMF(FILE* fout, double kbt) const;
    virtual void writeCount(FILE* fout) const;
    virtual bool getLogDerivative(const vector<double>& pos, vector<double>& deriv, int pbc = 0) const;
    virtual void writeGrad(const char* filename, double kbt) const;
    void writePMF(const char* filename, double kbt) const;
    void writeCount(const char* filename) const;
    virtual ~Histogram() {}
protected:
    size_t                  mNDim;
    vector<Axis>            mAxes;
    vector<size_t>          mAccu;
    size_t                  mGridSize;
    vector<double>          mWeightSum;
    vector<double>          mValue;
    vector<size_t>          mCount;
    vector<vector<double>>  mPointTable;
    void fillTable();
    size_t getGridSize() const {return this->mGridSize;}
};

Histogram::Histogram() {}

Histogram::Histogram(const vector<Axis>& ax):
mNDim(ax.size()), mAxes(ax), mAccu(mNDim) {
    mAccu[0] = 1;
    mGridSize = 1;
    for (size_t i = 0; i < mNDim; ++i) {
        mAccu[i] = (i == 0) ? 1 : (mAccu[i - 1] * mAxes[i - 1].bin());
        mGridSize *= mAxes[i].bin();
    }
    mWeightSum.assign(mGridSize, 0.0);
    mValue.assign(mGridSize, 0.0);
    mCount.assign(mGridSize, 0);
    fillTable();
}

void Histogram::fillTable() {
    vector<vector<double>> middlePoint(mNDim);
    for (size_t i = 0; i < mNDim; ++i) {
        middlePoint[i] = mAxes[i].middlePoint();
    }
    mPointTable.assign(mNDim, vector<double>(mGridSize, 0.0));
    for (size_t i = 0; i < mNDim; ++i) {
        size_t repeatAll = 1, repeatOne = 1;
        for (size_t j = i + 1; j < mNDim; ++j) {
            repeatOne *= middlePoint[j].size();
        }
        for (size_t j = 0; j < i; ++j) {
            repeatAll *= middlePoint[j].size();
        }
        const size_t in_i_sz = middlePoint[i].size();
        for (size_t l = 0; l < in_i_sz; ++l) {
            std::fill_n(begin(mPointTable[i]) + l * repeatOne, repeatOne, middlePoint[i][l]);
        }
        for (size_t k = 0; k < repeatAll - 1; ++k) {
        std::copy_n(begin(mPointTable[i]), repeatOne * in_i_sz,
                    begin(mPointTable[i]) + repeatOne * in_i_sz * (k + 1));
        }
    }
}

bool Histogram::isInGrid(const vector<double>& pos) const {
    auto it_val = pos.cbegin();
    auto it_ax = mAxes.cbegin();
    while (it_ax != mAxes.cend()) {
        if (!(it_ax->isInBoundary((*it_val)))) return false;
    }
    return true;
}

bool Histogram::index(const vector<double>& pos, vector<size_t>& idx) const {
    idx.assign(mNDim, 0);
    auto it_val = pos.cbegin();
    auto it_ax = mAxes.cbegin();
    auto it_idx = idx.begin();
    while (it_idx != idx.end()) {
        if (!(it_ax->index((*it_val), (*it_idx)))) return false;
    }
    return true;
}

bool Histogram::address(const vector<double>& pos, size_t& addr) const {
    addr = 0;
    for (size_t i = 0; i < mNDim; ++i) {
        size_t idx_i = 0;
        if (!(mAxes[i].index(pos[i], idx_i))) return false;
        addr += idx_i * mAccu[i];
    }
    return true;
}

bool Histogram::store(const vector<double>& pos, double value, double weight) {
    size_t addr = 0;
    bool inGrid = address(pos, addr);
//     printf("Address is: %lu\n", addr);
    if (inGrid) {
        mWeightSum[addr] += std::exp(weight);
        mValue[addr] = (mValue[addr] * static_cast<double>(mCount[addr]) + value * std::exp(weight))/ static_cast<double>(mCount[addr] + 1);
//         printf("Value is: %lf\n", mValue[addr]);
        ++(mCount[addr]);
    }
    return inGrid;
}

bool Histogram::getValue(const vector<double>& pos, double& res) const {
    size_t addr = 0;
    bool inGrid = address(pos, addr);
    if (inGrid) res = mValue[addr];
    return inGrid;
}

void Histogram::writePMF(FILE* fout, double kbt) const {
    fprintf(fout, "# %ld\n", mNDim);
    for (const auto &ax : mAxes) {
        fprintf(fout, "%s\n", ax.infoHeader(1).c_str());
    }
    vector<double> pos(mNDim, 0.0);
    vector<double> pmf(mGridSize, 0.0);
    double maxValue;
    bool first_non_zero_value = true;
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
        }
        size_t addr = 0;
        address(pos, addr);
        if (mCount[addr] != 0 && mValue[addr] != 0 && mWeightSum[addr] != 0) {
//             pmf[i] = -1.0 * kbt * std::log(static_cast<double>(mCount[addr]) * mValue[addr] / mWeightSum[addr]);
//             pmf[i] = -1.0 * kbt * std::log(mValue[addr] * mCount[addr]);
            pmf[i] = -1.0 * kbt * std::log(mValue[addr]);
            if (first_non_zero_value) {
                maxValue = pmf[i];
                first_non_zero_value = false;
            }
            maxValue = std::max(maxValue, pmf[i]);
        }
    }
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
        }
        size_t addr = 0;
        address(pos, addr);
        if (mCount[addr] == 0) {
            pmf[i] = maxValue;
        }
    }
    const double minValue = *std::min_element(pmf.begin(), pmf.end());
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
            fprintf(fout, " %.7f", pos[j]);
        }
        fprintf(fout, " %.7f\n", (pmf[i] - minValue));
    }
}

void Histogram::writeCount(FILE* fout) const {
    fprintf(fout, "# %ld\n", mNDim);
    for (const auto &ax : mAxes) {
        fprintf(fout, "%s\n", ax.infoHeader(1).c_str());
    }
    vector<double> pos(mNDim, 0.0);
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
            fprintf(fout, " %.7f", pos[j]);
        }
        size_t addr = 0;
        address(pos, addr);
        const size_t& count = mCount[addr];
        fprintf(fout, " %lu\n", count);
    }
}

void Histogram::writeGrad(const char* filename, double kbt) const {
    FILE* fout = fopen(filename, "w");
    fprintf(fout, "# %ld\n", mNDim);
    for (const auto &ax : mAxes) {
        fprintf(fout, "%s\n", ax.infoHeader(1).c_str());
    }
    vector<double> pos(mNDim, 0.0);
    vector<double> deriv(mNDim, 0.0);
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
            fprintf(fout, " %.7f", pos[j]);
        }
        getLogDerivative(pos, deriv, 0);
        for (size_t k = 0; k < mNDim; ++k) {
            fprintf(fout, " %.7f", -1.0 * kbt * deriv[k]);
        }
        fprintf(fout, "\n");
    }
    fclose(fout);
}

void Histogram::writePMF(const char* filename, double kbt) const {
    FILE* f_pmf = fopen(filename, "w");
    writePMF(f_pmf, kbt);
    fclose(f_pmf);
}

void Histogram::writeCount(const char* filename) const {
    FILE* f_count = fopen(filename, "w");
    writeCount(f_count);
    fclose(f_count);
}

bool Histogram::getLogDerivative(const vector<double>& pos, vector<double>& deriv, int pbc) const {
    // TODO: Handle pbc correctly
    size_t addr = 0;
    const bool inGrid = address(pos, addr);
    double value_this, value_prev, value_next, value_next2, value_prev2;
    if (inGrid == false) {
        return false;
    } else {
        getValue(pos, value_this);
        for (size_t i = 0; i < mNDim; ++i) {
            const double binWidth = mAxes[i].width();
            const size_t addr_first = addr - mAccu[i] * mAxes[i].index(pos[i]) + 0;
            const size_t addr_last = addr_first + mAccu[i] * (mAxes[i].bin() - 1);
            if (addr == addr_first) {
                if (pbc > 0) {
                    value_next = mValue[addr + mAccu[i]];
                    value_prev = mValue[addr_last];
                    if (value_next != 0 && value_prev != 0) {
                        deriv[i] = (std::log(value_next) - std::log(value_prev)) / (2 * binWidth);
                    }
                } else {
                    value_next = mValue[addr + mAccu[i]];
                    value_next2 = mValue[addr + mAccu[i] * 2];
                    if (value_next != 0 && value_this != 0 && value_next2 != 0) {
                        deriv[i] = (std::log(value_next2) * (-1.0) + std::log(value_next) * 4.0 - std::log(value_this) * 3.0) / (2.0 * binWidth);
                    }
                }
            } else if (addr == addr_last) {
                if (pbc > 0) {
                    value_prev = mValue[addr - mAccu[i]];
                    value_next = mValue[addr_first];
                    if (value_next != 0 && value_prev != 0) {
                        deriv[i] = (std::log(value_next) - std::log(value_prev)) / (2 * binWidth);
                    }
                } else {
                    value_prev = mValue[addr - mAccu[i]];
                    value_prev2 = mValue[addr - mAccu[i] * 2];
                    if (value_this != 0 && value_prev != 0 && value_prev2 != 0) {
                        deriv[i] = (std::log(value_this) * 3.0 - std::log(value_prev) * 4.0 + std::log(value_prev2)) / (2.0 * binWidth);
                    }
                }
            } else {
                value_prev = mValue[addr - mAccu[i]];
                value_next = mValue[addr + mAccu[i]];
                if (value_next != 0 && value_prev != 0) {
                    deriv[i] = (std::log(value_next) - std::log(value_prev)) / (2 * binWidth);
                }
            }
        }
        return true;
    }
}

// Accelerated MD histogram with improved reweighting
class AmdIRHistogram : public Histogram
{
public:
    AmdIRHistogram() {}
    AmdIRHistogram(const vector<Axis>& ax, size_t cumulantOrder, double kbt);
    virtual ~AmdIRHistogram() {}
    class Cumulant {
    public:
        Cumulant(size_t order): mOrder(order), mOrderAverage(order, 0) {}
        double calcCumulant(size_t k) const {
            if (k == 1) {
                return mOrderAverage[0];
            } else {
                double result = 0;
                for (size_t i = 1; i < k; ++i) {
                    result += mOrderAverage[i - 1] * std::pow(mOrderAverage[0], (k - i)) * std::pow((-1.0), (k - i)) * combination(k, i);
                }
                result += std::pow(-1.0, k) * std::pow(mOrderAverage[0], k);
                result += mOrderAverage[k - 1];
                return result;
            }
        }
        size_t mOrder;
        vector<double> mOrderAverage;
    };
    virtual bool store(const vector<double>& pos, double value = 1.0, double weight = 1.0);
    void dumpCumulants(const char outputName[]) const;
private:
    double mkbt;
    double mOrder;
    vector<Cumulant> mCumulantValue;
};

AmdIRHistogram::AmdIRHistogram(const vector<Axis>& ax, size_t cumulantOrder, double kbt): Histogram(ax), mkbt(kbt), mOrder(cumulantOrder) {
    mCumulantValue.resize(getGridSize(), Cumulant(cumulantOrder));
}

bool AmdIRHistogram::store(const vector<double>& pos, double value, double weight) {
    size_t addr = 0;
    bool inGrid = address(pos, addr);
    if (inGrid) {
        mWeightSum[addr] += std::exp(weight);
        double V = weight * mkbt;
        for (size_t i = 0; i < mOrder; ++i) {
            mCumulantValue[addr].mOrderAverage[i] = (mCumulantValue[addr].mOrderAverage[i] * mCount[addr] + std::pow(V, static_cast<double>(i + 1))) / (mCount[addr] + 1);
        }
        double expansion = 0;
        const double beta = 1 / mkbt;
        for (size_t i = 0; i < mOrder; ++i) {
            expansion += std::pow(beta, static_cast<double>(i+1)) * mCumulantValue[addr].calcCumulant(i + 1) / factorial(i + 1);
        }
        mValue[addr] = value * std::exp(expansion);
        ++(mCount[addr]);
    }
    return inGrid;
}

void AmdIRHistogram::dumpCumulants(const char outputName[]) const {
    FILE* fout = fopen(outputName, "w");
    fprintf(fout, "# %ld\n", mNDim);
    vector<double> pos(mNDim, 0.0);
    for (const auto &ax : mAxes) {
        fprintf(fout, "%s\n", ax.infoHeader(1).c_str());
    }
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
            fprintf(fout, " %.7f", pos[j]);
        }
        size_t addr = 0;
        address(pos, addr);
        for (size_t k = 0; k < mOrder; ++k) {
            fprintf(fout, " %.7f", mCumulantValue[addr].calcCumulant(k + 1));
        }
        fprintf(fout, "\n");
    }
    fclose(fout);
}

class AnharmonicityHistogram: public Histogram
{
public:
    AnharmonicityHistogram(const vector<Axis>& ax, double max_dV): Histogram(ax) {
        mdVAxis = Axis(0, max_dV, dVbins);
        mdV_cumulants.assign(mGridSize, vector<double>(2, 0.0));
        mdVCount.assign(mGridSize, vector<size_t>(dVbins, 0));
        anharm.assign(mGridSize, 0.0);
        mSdV.assign(mGridSize, 0.0);
    }
    bool storedV(const vector<double>& pos, double dV) {
        if (store(pos) == true) {
            size_t addr = 0;
            address(pos, addr);
            size_t dV_idx = 0;
            if (mdVAxis.index(dV, dV_idx) == true) {
                mdV_cumulants[addr][0] += dV;
                mdV_cumulants[addr][1] += dV * dV;
                mdVCount[addr][dV_idx]++;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    void calcAnharmonicity();
    void writeAnharmonicity(const string& filename);
    void dumpdVdistrib(const string& prefix) const;
private:
    static const size_t dVbins = 200;
    Axis mdVAxis;
    vector<vector<double>> mdV_cumulants;
    vector<vector<size_t>> mdVCount;
    vector<double> anharm;
    vector<double> mSdV;
//     double m_Smax;
};

void AnharmonicityHistogram::calcAnharmonicity() {
    const double pi = std::acos(-1.0);
    const double e = std::exp(1.0);
//     vector<double> dVmp = mdVAxis.middlePoint();
    vector<double> pos(mNDim, 0.0);
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
        }
        size_t addr = 0;
        address(pos, addr);
        size_t binCount = mCount[addr];
        if (binCount != 0) {
            double variance = mdV_cumulants[addr][1] / static_cast<double>(binCount) - (mdV_cumulants[addr][0] / static_cast<double>(binCount)) * (mdV_cumulants[addr][0] / static_cast<double>(binCount));
            double sdv = 0;
            for (size_t k = 0; k < dVbins; ++k) {
                if (mdVCount[addr][k] != 0) {
                    double p = static_cast<double>(mdVCount[addr][k]) / static_cast<double>(binCount);
                    sdv += p * std::log(p) * mdVAxis.width();
//                     cout << p << '\n';
                }
            }
            sdv = -1.0 * sdv;
            anharm[addr] = 0.5 * std::log(2.0 * pi * e * variance) - sdv;
            mSdV[addr] = sdv;
//             cout << "Smax: " << 0.5 * std::log(2.0 * pi * e * variance) << '\n';
//             cout << "Sdv: " << sdv << '\n';
        }
    }
    double maxAnharm = *std::max_element(anharm.begin(), anharm.end());
    for (size_t i = 0; i < mGridSize; ++i) {
        size_t binCount = std::accumulate(mdVCount[i].begin(), mdVCount[i].end(), 0);
        if (binCount == 0) {
            anharm[i] = maxAnharm + 1.0;
        }
    }
}

void AnharmonicityHistogram::writeAnharmonicity(const string& filename) {
    calcAnharmonicity();
    FILE* fout = fopen(filename.c_str(), "w");
    fprintf(fout, "# CV Anharm SdV C1 C2\n");
    vector<double> pos(mNDim, 0.0);
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
            fprintf(fout, " %.7f", pos[j]);
        }
        size_t addr = 0;
        address(pos, addr);
        fprintf(fout, " %.7f", anharm[addr]);
        fprintf(fout, " %.7f", mSdV[addr]);
        fprintf(fout, " %.7f %.7f\n", mdV_cumulants[addr][0] / static_cast<double>(mCount[addr]), mdV_cumulants[addr][1] / static_cast<double>(mCount[addr]));
    }
    fclose(fout);
}

void AnharmonicityHistogram::dumpdVdistrib(const string& prefix) const {
    vector<double> pos(mNDim, 0.0);
    vector<double> dVmp = mdVAxis.middlePoint();
    for (size_t i = 0; i < mGridSize; ++i) {
        for (size_t j = 0; j < mNDim; ++j) {
            pos[j] = mPointTable[j][i];
        }
        size_t addr = 0;
        address(pos, addr);
        string filename = prefix + std::to_string(addr) + ".dat";
        FILE* fout = fopen(filename.c_str(), "w");
        fprintf(fout, "# dV at ");
        for (size_t j = 0; j < mNDim; ++j) {
            fprintf(fout, "%.7f ", pos[j]);
        }
        fprintf(fout, "\n");
        fprintf(fout, "# Anharmonicity: %.7f \n", anharm[addr]);
        for (size_t k = 0; k < dVbins; ++k) {
            fprintf(fout, "%.7lf %lu\n", dVmp[k], mdVCount[addr][k]);
        }
        fclose(fout);
    }
}

#endif
