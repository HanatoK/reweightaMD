#include <vector>
#include <cmath>
#include <cstdio>
#include <string>

using std::vector;
using std::string;

class Statistic
{
public:
    Statistic(size_t order = 1): m_order(order), m_count(order, 0), m_orderavg(order, 0) {}
    void store(double x) {
        for (size_t i = 0; i < m_order; ++i) {
            m_orderavg[i] = m_orderavg[i] * double(m_count[i]) + std::pow(x, (i+1));
            ++m_count[i];
            if (m_count[i] != 0) {
                m_orderavg[i] /= double(m_count[i]);
            }
        }
    }
    double get(size_t k) const {
        return m_orderavg[k-1];
    }
    void writeResults(const string& filename) const;
private:
    size_t m_order;
    vector<size_t> m_count;
    vector<double> m_orderavg; 
};

void Statistic::writeResults(const string& filename) const {
    FILE* f_out = fopen(filename.c_str(), "w");
    fprintf(f_out, "# Count ");
    for (size_t i = 0; i < m_order; ++i) {
        string tmp = "Order" + std::to_string(i+1);
        fprintf(f_out, "%s ", tmp.c_str());
    }
    fprintf(f_out, "\n");
    fprintf(f_out, " %lu", m_order);
    for (size_t i = 0; i < m_order; ++i) {
        fprintf(f_out, " %.7lf", m_orderavg[i]);
    }
    fclose(f_out);
}
