# Makefile for reweightaMD

CXX=g++

all: histo

histo: histo.cpp
	$(CXX) histo.cpp -o histo -O3 -Wall -Wextra -std=gnu++11

clean:
	rm histo
