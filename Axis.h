#ifndef AXIS_H
#define AXIS_H
#include <cstddef>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#include <cmath>

using std::string;
using std::vector;

class Axis
{
public:
    Axis();
    Axis(double lowerBound, double upperBound, size_t bin);
    bool isInitialized() const;
    double width() const;
    size_t bin() const;
    bool isInBoundary(double value) const;
    bool index(double x, size_t& idx) const;
    double index(double x) const;
    string infoHeader(short pbc) const;
    vector<double> middlePoint() const;
private:
    double          mLowerBound;
    double          mUpperBound;
    size_t          mBin;
    double          mWidth;
};

Axis::Axis():
mLowerBound(0.0), mUpperBound(0.0), mBin(0), mWidth(0.0) {}

Axis::Axis(double lowerBound, double upperBound, size_t bin):
mLowerBound(lowerBound), mUpperBound(upperBound), mBin(bin), mWidth((upperBound - lowerBound) / static_cast<double>(bin)) {}

bool Axis::isInitialized() const {
    return static_cast<bool>(mBin);
}

double Axis::width() const {
    return mWidth;
}

size_t Axis::bin() const {
    return mBin;
}

bool Axis::isInBoundary(double value) const {
    if (value < mLowerBound || value > mUpperBound) {
        return false;
    } else {
        return true;
    }
}

bool Axis::index(double x, size_t& idx) const {
    if (!isInBoundary(x)) {
        return false;
    }
    idx = std::floor((x - mLowerBound) / mWidth);
    if (idx == mBin) {
        --idx;
    }
    return true;
}

double Axis::index(double x) const {
    size_t idx = std::floor((x - mLowerBound) / mWidth);
    idx = (idx == mBin) ? (mBin - 1) : idx;
    return idx;
}

string Axis::infoHeader(short pbc) const {
    std::stringstream ss;
    ss << "# " << std::fixed << std::setprecision(9)
       << mLowerBound << ' ' << mWidth << ' ' << mBin << ' ' << pbc;
    return ss.str();
}

vector<double> Axis::middlePoint() const {
    double tmp = mLowerBound - 0.5 * mWidth;
    vector<double> result(mBin, 0.0);
    for(auto &i : result) {
        tmp += mWidth;
        i = tmp;
    }
    return result;
}

#endif
