#include "Axis.h"
#include "ReweightGrid.h"
#include "SimpleStat.h"
#include <fstream>
#include <vector>
#include <string>
#include <cstdio>
#include <iostream>

using std::cout;
using std::ifstream;
using std::vector;
using std::string;

void info() {
    cout << "Compile date: " << __DATE__ << '\n';
    cout << "Compile time: " << __TIME__ << std::endl;
}

vector<Axis> parse_cv_header(string config) {
    ifstream headerFileStream(config.c_str());
    string line;
    vector<Axis> axes;
    size_t ndim = 0;
    double grid_min;
    double width;
    size_t nbin;
    int pbc;
    while (std::getline(headerFileStream, line)) {
        if (sscanf(line.c_str(), "# %lf %lf %lu %d", &grid_min, &width, &nbin, &pbc) == 4) {
            double grid_max = grid_min + width * static_cast<double>(nbin);
            if (ndim == 0) {
                std::cerr << "Dimension not defined!" << '\n';
            }
            axes.push_back(Axis(grid_min, grid_max, nbin));
        } else if (sscanf(line.c_str(), "# %lu", &ndim) == 1) {
            cout << "Dimension: " << ndim << '\n';
        }
    }
    return axes;
}

int main(int argc, char* argv[]) {
    info();
    if (argc <= 2) {
        std::cerr << "ERROR!" << '\n';
        return 1;
    }
    double kbt = 300 * 0.0019872041;
    vector<Axis> colvar = parse_cv_header(string(argv[1]));
    Histogram histo(colvar);
    const int N=4;
    AmdIRHistogram irhisto1(colvar, 1, kbt);
    AmdIRHistogram irhisto2(colvar, 2, kbt);
    AmdIRHistogram irhisto3(colvar, 3, kbt);
    AmdIRHistogram irhistoN(colvar, N, kbt);
    ifstream trajFileStream(argv[2]);
    Axis dVrange(0, 20.0, 200);
    AnharmonicityHistogram dVdistribution(colvar, 12.0);
    string line;
    unsigned long int step;
    vector<double> cvValue(1, 0);
    vector<double> cvExtended(1, 0);
    vector<double> cvAppliedForce(1, 0);
    double dV;
    Statistic stat(2);
    while (std::getline(trajFileStream, line)) {
        if (sscanf(line.c_str(), "%lu %lf %lf %lf %lf", &step, &(cvValue[0]), &(cvExtended[0]), &(cvAppliedForce[0]), &dV) == 5) {
//             if (dV > 0) {
                histo.store(cvValue, 1.0, dV / kbt);
                irhisto1.store(cvValue, 1.0, dV / kbt);
                irhisto2.store(cvValue, 1.0, dV / kbt);
                irhisto3.store(cvValue, 1.0, dV / kbt);
                irhistoN.store(cvValue, 1.0, dV / kbt);
                dVdistribution.storedV(cvValue, dV);
                stat.store(dV);
//             }
        } else if (sscanf(line.c_str(), "%lu %lf %lf %lf", &step, &(cvValue[0]), &(cvAppliedForce[0]), &dV) == 4) {
//             if (dV > 0) {
                histo.store(cvValue, 1.0, dV / kbt);
                irhisto1.store(cvValue, 1.0, dV / kbt);
                irhisto2.store(cvValue, 1.0, dV / kbt);
                irhisto3.store(cvValue, 1.0, dV / kbt);
                irhistoN.store(cvValue, 1.0, dV / kbt);
                dVdistribution.storedV(cvValue, dV);
                stat.store(dV);
//             }
        } else if (sscanf(line.c_str(), "%lu %lf %lf", &step, &(cvValue[0]), &dV) == 3) {
//             if (dV > 0) {
                histo.store(cvValue, 1.0, dV / kbt);
                irhisto1.store(cvValue, 1.0, dV / kbt);
                irhisto2.store(cvValue, 1.0, dV / kbt);
                irhisto3.store(cvValue, 1.0, dV / kbt);
                irhistoN.store(cvValue, 1.0, dV / kbt);
                dVdistribution.storedV(cvValue, dV);
                stat.store(dV);
//             }
        }
    }
    FILE* fp_count = fopen("alad_amd.count", "w");
    FILE* fp_pmf = fopen("alad_amd.pmf", "w");
    FILE* fp_count_ir = fopen("alad_amd_ir.count", "w");
    FILE* fp_pmf_ir = fopen("alad_amd_ir1.pmf", "w");
//     FILE* fp_dv_histo = fopen("dV_histo.dat", "w");
    FILE* fp_pmf_ir3 = fopen("alad_amd_ir3.pmf", "w");
    FILE* fp_pmf_ir2 = fopen("alad_amd_ir2.pmf", "w");
    string irN = "alad_amd_ir" + std::to_string(N) + ".pmf";
    FILE* fp_pmf_irN = fopen(irN.c_str(), "w");
    histo.writeCount(fp_count);
    histo.writePMF(fp_pmf, kbt);
    irhisto1.writePMF(fp_pmf_ir, kbt);
    irhisto1.writeCount(fp_count_ir);
    irhisto2.writePMF(fp_pmf_ir2, kbt);
    irhisto3.writePMF(fp_pmf_ir3, kbt);
//     irhisto3.dumpCumulants("alad_cumulants.dat");
//     irhisto3.writeGrad("alad_amd_ir.grad", kbt);
    irhistoN.writePMF(fp_pmf_irN, kbt);
    irhistoN.dumpCumulants("alad_cumulants.dat");
    double var = stat.get(2) - stat.get(1) * stat.get(1);
    cout << "Variance of dV: " << var << '\n';
    stat.writeResults("dVstat.dat");
//     dVdistribution.writeCount(fp_dv_histo);
    dVdistribution.writeAnharmonicity("dV_anharm.dat");
    dVdistribution.dumpdVdistrib("dV_histo");
    fclose(fp_count);
    fclose(fp_pmf);
    fclose(fp_count_ir);
    fclose(fp_pmf_ir);
//     fclose(fp_dv_histo);
    fclose(fp_pmf_ir2);
    fclose(fp_pmf_ir3);
    fclose(fp_pmf_irN);
    return 0;
}
